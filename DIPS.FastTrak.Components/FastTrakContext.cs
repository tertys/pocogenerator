using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using DIPS.FastTrak.Database;
using DIPS.FastTrak.Services;
using Optional.Unsafe;
using UlrikenUtils;
// ReSharper disable InconsistentNaming
// ReSharper disable CheckNamespace

namespace DIPS.FastTrak.Components
{
	public class FastTrakContext : DbContext, IFastTrakContext
	{
		public FastTrakContext(IConnectionService connectionService)
			: base(GetSqlConnection(connectionService), true){}

		private static SqlConnection GetSqlConnection(IConnectionService connectionService)
		{
			var connection = connectionService.Connect();
			Ensure.That(connection.Success, "Unable to create a connection to the database!").IsTrue();
			Ensure.That(connection.Value.HasValue, "No SqlConnection generated, this is fatal").IsTrue();
			return connection.Value.ValueOrDefault();
		}

		public virtual Result<List<GetPopulationResult>> GetPopulation(int studyId, string sqlText)
			=> Do.Try(()=>Database.SqlQuery<GetPopulationResult>($"{sqlText}".Replace(":", "@"), new SqlParameter("StudyId", studyId)).ToList());

		public Result<List<GetMyRolesResult>> GetMyRoles ()
			=> Do.Try(()=>Database.SqlQuery<GetMyRolesResult>("[AccessCtrl].[GetMyRoles] ").ToList());

		public Result<List<GetClinDataResult>> GetClinData (int StudyId,int PersonId)
			=> Do.Try(()=>Database.SqlQuery<GetClinDataResult>("[CRF].[GetClinData] @StudyId,@PersonId",new SqlParameter("StudyId", StudyId),new SqlParameter("PersonId", PersonId)).ToList());

		public Result<List<GetClinFormListResult>> GetClinFormList (int StudyId,int PersonId,bool IncludeArchived)
			=> Do.Try(()=>Database.SqlQuery<GetClinFormListResult>("[CRF].[GetClinFormList] @StudyId,@PersonId,@IncludeArchived",new SqlParameter("StudyId", StudyId),new SqlParameter("PersonId", PersonId),new SqlParameter("IncludeArchived", IncludeArchived)).ToList());

		public Result<List<GetFormActionsResult>> GetFormActions (int FormId)
			=> Do.Try(()=>Database.SqlQuery<GetFormActionsResult>("[CRF].[GetFormActions] @FormId",new SqlParameter("FormId", FormId)).ToList());

		public Result<List<GetFormItemsResult>> GetFormItems (int FormId)
			=> Do.Try(()=>Database.SqlQuery<GetFormItemsResult>("[CRF].[GetFormItems] @FormId",new SqlParameter("FormId", FormId)).ToList());

		public Result<List<GetFormPagesResult>> GetFormPages (int FormId)
			=> Do.Try(()=>Database.SqlQuery<GetFormPagesResult>("[CRF].[GetFormPages] @FormId",new SqlParameter("FormId", FormId)).ToList());

		public Result<List<GetMetaFormsResult>> GetMetaForms (int StudyId)
			=> Do.Try(()=>Database.SqlQuery<GetMetaFormsResult>("[CRF].[GetMetaForms] @StudyId",new SqlParameter("StudyId", StudyId)).ToList());

		public Result<List<GetPageActionsResult>> GetPageActions (int FormId)
			=> Do.Try(()=>Database.SqlQuery<GetPageActionsResult>("[CRF].[GetPageActions] @FormId",new SqlParameter("FormId", FormId)).ToList());

		public Result<List<GetSingleFormDataResult>> GetSingleFormData (int ClinFormId,DateTime? ChangedAfter)
			=> Do.Try(()=>Database.SqlQuery<GetSingleFormDataResult>("[CRF].[GetSingleFormData] @ClinFormId,@ChangedAfter",new SqlParameter("ClinFormId", ClinFormId),ChangedAfter.HasValue ? new SqlParameter("ChangedAfter",ChangedAfter) : new SqlParameter("ChangedAfter", DBNull.Value)).ToList());

		public Result<List<GetStudyFormResult>> GetStudyForm (int StudyId,int FormId)
			=> Do.Try(()=>Database.SqlQuery<GetStudyFormResult>("[CRF].[GetStudyForm] @StudyId,@FormId",new SqlParameter("StudyId", StudyId),new SqlParameter("FormId", FormId)).ToList());

		public Result<List<GetStudyFormItemsResult>> GetStudyFormItems (int StudyId)
			=> Do.Try(()=>Database.SqlQuery<GetStudyFormItemsResult>("[CRF].[GetStudyFormItems] @StudyId",new SqlParameter("StudyId", StudyId)).ToList());

		public Result<List<AddClinFormResult>> AddClinForm (int SessId,int PersonId,int FormId,DateTime EventTime)
			=> Do.Try(()=>Database.SqlQuery<AddClinFormResult>("[dbo].[AddClinForm] @SessId,@PersonId,@FormId,@EventTime",new SqlParameter("SessId", SessId),new SqlParameter("PersonId", PersonId),new SqlParameter("FormId", FormId),new SqlParameter("EventTime", EventTime)).ToList());

		public Result<int> AddMyself (DateTime DOB,int GenderId,string FstName,string MidName,string LstName,string NationalId)
			=> Do.Try(()=>Database.ExecuteSqlCommand("[dbo].[AddMyself] @DOB,@GenderId,@FstName,@MidName,@LstName,@NationalId",new SqlParameter("DOB", DOB),new SqlParameter("GenderId", GenderId),new SqlParameter("FstName", FstName),new SqlParameter("MidName", MidName),new SqlParameter("LstName", LstName),new SqlParameter("NationalId", NationalId)));

		public Result<List<AddPersonResult>> AddPerson (DateTime DOB,string FstName,string MidName,string LstName,int GenderId,string NationalId)
			=> Do.Try(()=>Database.SqlQuery<AddPersonResult>("[dbo].[AddPerson] @DOB,@FstName,@MidName,@LstName,@GenderId,@NationalId",new SqlParameter("DOB", DOB),new SqlParameter("FstName", FstName),new SqlParameter("MidName", MidName),new SqlParameter("LstName", LstName),new SqlParameter("GenderId", GenderId),NationalId.HasValue ? new SqlParameter("NationalId",NationalId) : new SqlParameter("NationalId", DBNull.Value)).ToList());

		public Result<List<AddSessionResult>> AddSession (int StudyId,string CompName,string CompUser,DateTime CompTime,string AppVer)
			=> Do.Try(()=>Database.SqlQuery<AddSessionResult>("[dbo].[AddSession] @StudyId,@CompName,@CompUser,@CompTime,@AppVer",new SqlParameter("StudyId", StudyId),new SqlParameter("CompName", CompName),new SqlParameter("CompUser", CompUser),new SqlParameter("CompTime", CompTime),new SqlParameter("AppVer", AppVer)).ToList());

		public Result<List<GetCaseListResult>> GetCaseList (int StudyId)
			=> Do.Try(()=>Database.SqlQuery<GetCaseListResult>("[dbo].[GetCaseList] @StudyId",new SqlParameter("StudyId", StudyId)).ToList());

		public Result<List<GetClinFormsResult>> GetClinForms (int StudyId,int PersonId,int? ClinFormId)
			=> Do.Try(()=>Database.SqlQuery<GetClinFormsResult>("[dbo].[GetClinForms] @StudyId,@PersonId,@ClinFormId",new SqlParameter("StudyId", StudyId),new SqlParameter("PersonId", PersonId),ClinFormId.HasValue ? new SqlParameter("ClinFormId",ClinFormId) : new SqlParameter("ClinFormId", DBNull.Value)).ToList());

		public Result<List<GetDatabaseInfoResult>> GetDatabaseInfo ()
			=> Do.Try(()=>Database.SqlQuery<GetDatabaseInfoResult>("[dbo].[GetDatabaseInfo] ").ToList());

		public Result<List<GetDrugTreatDetailsResult>> GetDrugTreatDetails (int TreatId)
			=> Do.Try(()=>Database.SqlQuery<GetDrugTreatDetailsResult>("[dbo].[GetDrugTreatDetails] @TreatId",new SqlParameter("TreatId", TreatId)).ToList());

		public Result<List<GetLabDataResult>> GetLabData (int PersonId)
			=> Do.Try(()=>Database.SqlQuery<GetLabDataResult>("[dbo].[GetLabData] @PersonId",new SqlParameter("PersonId", PersonId)).ToList());

		public Result<List<GetLabNamesResult>> GetLabNames ()
			=> Do.Try(()=>Database.SqlQuery<GetLabNamesResult>("[dbo].[GetLabNames] ").ToList());

		public Result<List<GetPatientsResult>> GetPatients ()
			=> Do.Try(()=>Database.SqlQuery<GetPatientsResult>("[dbo].[GetPatients] ").ToList());

		public Result<List<GetPersonDetailsResult>> GetPersonDetails (int SessId,int PersonId)
			=> Do.Try(()=>Database.SqlQuery<GetPersonDetailsResult>("[dbo].[GetPersonDetails] @SessId,@PersonId",new SqlParameter("SessId", SessId),new SqlParameter("PersonId", PersonId)).ToList());

		public Result<List<GetStudyAndUserResult>> GetStudyAndUser (string StudyName)
			=> Do.Try(()=>Database.SqlQuery<GetStudyAndUserResult>("[dbo].[GetStudyAndUser] @StudyName",new SqlParameter("StudyName", StudyName)).ToList());

		public Result<List<GetStudyGroupsResult>> GetStudyGroups (int StudyId,int? UserId)
			=> Do.Try(()=>Database.SqlQuery<GetStudyGroupsResult>("[dbo].[GetStudyGroups] @StudyId,@UserId",new SqlParameter("StudyId", StudyId),UserId.HasValue ? new SqlParameter("UserId",UserId) : new SqlParameter("UserId", DBNull.Value)).ToList());

		public Result<List<GetStudyNamesResult>> GetStudyNames ()
			=> Do.Try(()=>Database.SqlQuery<GetStudyNamesResult>("[dbo].[GetStudyNames] ").ToList());

		public Result<List<GetUserDetailsResult>> GetUserDetails (string UserNameOrId,int? StudyId)
			=> Do.Try(()=>Database.SqlQuery<GetUserDetailsResult>("[dbo].[GetUserDetails] @UserNameOrId,@StudyId",new SqlParameter("UserNameOrId", UserNameOrId),StudyId.HasValue ? new SqlParameter("StudyId",StudyId) : new SqlParameter("StudyId", DBNull.Value)).ToList());

		public Result<int> UpdateCaseGroup (int StudyId,int PersonId,int GroupId)
			=> Do.Try(()=>Database.ExecuteSqlCommand("[dbo].[UpdateCaseGroup] @StudyId,@PersonId,@GroupId",new SqlParameter("StudyId", StudyId),new SqlParameter("PersonId", PersonId),new SqlParameter("GroupId", GroupId)));

		public Result<int> UpdateClinFormSetTime (int ClinFormId,DateTime NewEventTime)
			=> Do.Try(()=>Database.ExecuteSqlCommand("[dbo].[UpdateClinFormSetTime] @ClinFormId,@NewEventTime",new SqlParameter("ClinFormId", ClinFormId),new SqlParameter("NewEventTime", NewEventTime)));

		public Result<int> UpdatePerson (int PersonId,DateTime DOB,byte GenderId,string FstName,string LstName,string NationalId)
			=> Do.Try(()=>Database.ExecuteSqlCommand("[dbo].[UpdatePerson] @PersonId,@DOB,@GenderId,@FstName,@LstName,@NationalId",new SqlParameter("PersonId", PersonId),new SqlParameter("DOB", DOB),new SqlParameter("GenderId", GenderId),new SqlParameter("FstName", FstName),new SqlParameter("LstName", LstName),new SqlParameter("NationalId", NationalId)));

		public Result<List<GetStudyPopulationsResult>> GetStudyPopulations (int StudyId,int? DbVer)
			=> Do.Try(()=>Database.SqlQuery<GetStudyPopulationsResult>("[Populations].[GetStudyPopulations] @StudyId,@DbVer",new SqlParameter("StudyId", StudyId),DbVer.HasValue ? new SqlParameter("DbVer",DbVer) : new SqlParameter("DbVer", DBNull.Value)).ToList());
   }
}