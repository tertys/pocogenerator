using System;
namespace DIPS.FastTrak.Database
{
	public class GetStudyAnswersResult
	{
		public int ItemId { get; set; }
		public int OrderNumber { get; set; }
		public int AnswerId { get; set; }
		public string OptionText { get; set; }
		public string VerboseText { get; set; }
		public string ICD10 { get; set; }
		public Double? Score { get; set; }
		public string ShortCode { get; set; }
		public DateTime? LastUpdate { get; set; }
		public string HtmlColor { get; set; }
	}
}