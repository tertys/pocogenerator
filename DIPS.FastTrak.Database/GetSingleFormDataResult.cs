using System;
namespace DIPS.FastTrak.Database
{
	public class GetSingleFormDataResult
	{
		public int RowId { get; set; }
		public int EventId { get; set; }
		public int EventNum { get; set; }
		public DateTime? EventTime { get; set; }
		public int ItemId { get; set; }
		public string VarName { get; set; }
		public Decimal? Quantity { get; set; }
		public int? EnumVal { get; set; }
		public DateTime? DTVal { get; set; }
		public string TextVal { get; set; }
		public DateTime ObsDate { get; set; }
		public int Locked { get; set; }
		public byte ChangeCount { get; set; }
	}
}