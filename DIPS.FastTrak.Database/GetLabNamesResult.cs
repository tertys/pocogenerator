using System;
namespace DIPS.FastTrak.Database
{
	public class GetLabNamesResult
	{
		public int LabCodeId { get; set; }
		public string LabName { get; set; }
		public string UnitStr { get; set; }
	}
}