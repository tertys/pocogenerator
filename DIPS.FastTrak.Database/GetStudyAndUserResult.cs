using System;
namespace DIPS.FastTrak.Database
{
	public class GetStudyAndUserResult
	{
		public int StudyId { get; set; }
		public int? PersonId { get; set; }
		public DateTime? DOB { get; set; }
		public string FstName { get; set; }
		public string MidName { get; set; }
		public string LstName { get; set; }
		public byte? GenderId { get; set; }
		public string NationalId { get; set; }
		public int? HPRNo { get; set; }
		public string CAVE { get; set; }
		public string FullName { get; set; }
		public string Initials { get; set; }
		public int? NatGenderId { get; set; }
		public string Signature { get; set; }
		public string BestId { get; set; }
		public string ReverseName { get; set; }
		public string GSM { get; set; }
		public DateTime? CreatedAt { get; set; }
		public int? CreatedBy { get; set; }
		public Guid? guid { get; set; }
		public string NB { get; set; }
		public string Reservations { get; set; }
		public string Allergies { get; set; }
		public DateTime? DeceasedDate { get; set; }
		public bool? DeceasedInd { get; set; }
		public string StreetAddress { get; set; }
		public string PostalCode { get; set; }
		public string City { get; set; }
		public string KommuneNr { get; set; }
		public string KommuneNavn { get; set; }
		public string FylkeNr { get; set; }
		public string FylkeNavn { get; set; }
		public bool? TestCase { get; set; }
		public int? ProfId { get; set; }
		public string ProfName { get; set; }
		public string OID9060 { get; set; }
		public int? ProfLevel { get; set; }
		public string ProfDescription { get; set; }
		public string ProfType { get; set; }
		public int? CenterId { get; set; }
		public string CenterName { get; set; }
		public string CenterPostCode { get; set; }
		public string CenterCity { get; set; }
		public string CenterAddress { get; set; }
		public string CenterPhone { get; set; }
		public byte? BlockRules { get; set; }
		public string DatabaseId { get; set; }
		public byte? CenterActive { get; set; }
		public int? GroupId { get; set; }
		public string GroupName { get; set; }
		public int? StudyGroupId { get; set; }
		public int? GroupActive { get; set; }
		public int? BedCount { get; set; }
		public DateTime? DisabledAt { get; set; }
		public int? DisabledBy { get; set; }
		public int? OrgId { get; set; }
		public bool? ShowMyGroup { get; set; }
		public int? CaseList { get; set; }
		public int? UserId { get; set; }
		public string UserName { get; set; }
	}
}