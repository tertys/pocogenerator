using System;

namespace DIPS.FastTrak.Database
{
	public class GetPopulationResult
{
		public int PersonId { get; set; }
		public DateTime DOB { get; set; }
		public string FullName { get; set; }
		public string GroupName { get; set; }
		public string InfoText { get; set; }
	}
}