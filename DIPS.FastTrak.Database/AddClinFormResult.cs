using System;
namespace DIPS.FastTrak.Database
{
	public class AddClinFormResult
	{
		public int? EventId { get; set; }
		public int? ClinFormId { get; set; }
		public int? EventNum { get; set; }
	}
}