using System;
namespace DIPS.FastTrak.Database
{
	public class GetPageActionsResult
	{
		public int MasterId { get; set; }
		public int? PageNumber { get; set; }
		public int? ComparisonType { get; set; }
		public int PageConditionId { get; set; }
		public int? EnumVal { get; set; }
	}
}