using System;
namespace DIPS.FastTrak.Database
{
	public class GetStudyPopulationsResult
	{
		public int ProcId { get; set; }
		public string ProcName { get; set; }
		public string ProcParams { get; set; }
		public string StudyName { get; set; }
		public string HelpText { get; set; }
		public string InfoCaption { get; set; }
		public string ProcGroup { get; set; }
		public string ProcTitle { get; set; }
		public string ProcDesc { get; set; }
		public string ProcSourceCode { get; set; }
		public string SqlText { get; set; }
		public int? MinVersion { get; set; }
		public int? MaxVersion { get; set; }
		public DateTime? DisabledAt { get; set; }
		public int StudyId { get; set; }
		public string StudName { get; set; }
		public byte LabModule { get; set; }
		public byte ProblemModule { get; set; }
		public byte DrugModule { get; set; }
		public byte CDSSModule { get; set; }
		public bool FixedStatus { get; set; }
	}
}