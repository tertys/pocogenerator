using System;
namespace DIPS.FastTrak.Database
{
	public class GetDatabaseInfoResult
	{
		public int? UserId { get; set; }
		public string UserName { get; set; }
		public string DatabaseName { get; set; }
		public int? DatabaseVersion { get; set; }
		public int ServerType { get; set; }
		public DateTime ServerTime { get; set; }
		public string ServerVersion { get; set; }
		public int EventScale { get; set; }
		public DateTime? NullDateTime { get; set; }
	}
}