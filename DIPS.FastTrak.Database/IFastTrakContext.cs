using System;
using System.Collections.Generic;
using UlrikenUtils;

namespace DIPS.FastTrak.Database
{
	public interface IFastTrakContext
	{
		Result<List<GetPopulationResult>> GetPopulation(int studyId, string sqlText);
		Result<List<GetMyRolesResult>> GetMyRoles ();
		Result<List<GetClinDataResult>> GetClinData (int StudyId,int PersonId);
		Result<List<GetClinFormListResult>> GetClinFormList (int StudyId,int PersonId,bool IncludeArchived);
		Result<List<GetFormActionsResult>> GetFormActions (int FormId);
		Result<List<GetFormItemsResult>> GetFormItems (int FormId);
		Result<List<GetFormPagesResult>> GetFormPages (int FormId);
		Result<List<GetMetaFormsResult>> GetMetaForms (int StudyId);
		Result<List<GetPageActionsResult>> GetPageActions (int FormId);
		Result<List<GetSingleFormDataResult>> GetSingleFormData (int ClinFormId,DateTime? ChangedAfter);
		Result<List<GetStudyFormResult>> GetStudyForm (int StudyId,int FormId);
		Result<List<GetStudyFormItemsResult>> GetStudyFormItems (int StudyId);
		Result<List<AddClinFormResult>> AddClinForm (int SessId,int PersonId,int FormId,DateTime EventTime);
		Result<int> AddMyself (DateTime DOB,int GenderId,string FstName,string MidName,string LstName,string NationalId);
		Result<List<AddPersonResult>> AddPerson (DateTime DOB,string FstName,string MidName,string LstName,int GenderId,string NationalId);
		Result<List<AddSessionResult>> AddSession (int StudyId,string CompName,string CompUser,DateTime CompTime,string AppVer);
		Result<List<GetCaseListResult>> GetCaseList (int StudyId);
		Result<List<GetClinFormsResult>> GetClinForms (int StudyId,int PersonId,int? ClinFormId);
		Result<List<GetDatabaseInfoResult>> GetDatabaseInfo ();
		Result<List<GetDrugTreatDetailsResult>> GetDrugTreatDetails (int TreatId);
		Result<List<GetLabDataResult>> GetLabData (int PersonId);
		Result<List<GetLabNamesResult>> GetLabNames ();
		Result<List<GetPatientsResult>> GetPatients ();
		Result<List<GetPersonDetailsResult>> GetPersonDetails (int SessId,int PersonId);
		Result<List<GetStudyAndUserResult>> GetStudyAndUser (string StudyName);
		Result<List<GetStudyGroupsResult>> GetStudyGroups (int StudyId,int? UserId);
		Result<List<GetStudyNamesResult>> GetStudyNames ();
		Result<List<GetUserDetailsResult>> GetUserDetails (string UserNameOrId,int? StudyId);
		Result<int> UpdateCaseGroup (int StudyId,int PersonId,int GroupId);
		Result<int> UpdateClinFormSetTime (int ClinFormId,DateTime NewEventTime);
		Result<int> UpdatePerson (int PersonId,DateTime DOB,byte GenderId,string FstName,string LstName,string NationalId);
		Result<List<GetStudyPopulationsResult>> GetStudyPopulations (int StudyId,int? DbVer);
	}
}