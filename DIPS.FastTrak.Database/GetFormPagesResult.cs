using System;
namespace DIPS.FastTrak.Database
{
	public class GetFormPagesResult
	{
		public int PageId { get; set; }
		public int FormId { get; set; }
		public int PageNumber { get; set; }
		public string PageTitle { get; set; }
		public string PageIntroduction { get; set; }
		public DateTime LastUpdate { get; set; }
	}
}