using System;
namespace DIPS.FastTrak.Database
{
	public class GetStudyFormItemsResult
	{
		public int FormId { get; set; }
		public int ItemId { get; set; }
		public int OrderNumber { get; set; }
		public string ItemHeader { get; set; }
		public string ItemText { get; set; }
		public string VarName { get; set; }
		public string UnitStr { get; set; }
		public int ItemType { get; set; }
		public string MinExpression { get; set; }
		public string MaxExpression { get; set; }
		public byte? Decimals { get; set; }
		public int FormItemId { get; set; }
	}
}