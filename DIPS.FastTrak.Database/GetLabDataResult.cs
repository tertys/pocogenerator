using System;
namespace DIPS.FastTrak.Database
{
	public class GetLabDataResult
	{
		public int ResultId { get; set; }
		public DateTime LabDate { get; set; }
		public int LabCodeId { get; set; }
		public string LabName { get; set; }
		public Double? NumResult { get; set; }
		public string UnitStr { get; set; }
		public byte? DevResult { get; set; }
		public string TxtResult { get; set; }
		public string ArithmeticComp { get; set; }
		public string Comment { get; set; }
		public string RefInterval { get; set; }
		public int? SignedBy { get; set; }
		public DateTime? SignedAt { get; set; }
		public string Initials { get; set; }
		public string Signature { get; set; }
		public string FullName { get; set; }
	}
}