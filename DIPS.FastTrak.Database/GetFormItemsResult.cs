using System;
namespace DIPS.FastTrak.Database
{
	public class GetFormItemsResult
	{
		public int FormItemId { get; set; }
		public int FormId { get; set; }
		public int OrderNumber { get; set; }
		public int ItemId { get; set; }
		public string VarName { get; set; }
		public string LabName { get; set; }
		public int ItemType { get; set; }
		public string UnitStr { get; set; }
		public Decimal? MinNormal { get; set; }
		public Decimal? MaxNormal { get; set; }
		public int? ThreadTypeId { get; set; }
		public bool? Multiline { get; set; }
		public bool? ExcludeFromText { get; set; }
		public bool? ExcludeCaption { get; set; }
		public string ItemHeader { get; set; }
		public string ItemText { get; set; }
		public string ItemHelp { get; set; }
		public bool? Optional { get; set; }
		public bool? ReadOnly { get; set; }
		public byte? CarryForward { get; set; }
		public string MinExpression { get; set; }
		public string MaxExpression { get; set; }
		public byte? Decimals { get; set; }
		public string Expression { get; set; }
		public string FormatStr { get; set; }
		public string QuantityFormatStr { get; set; }
		public byte? Highlight { get; set; }
		public byte? ClearStrategy { get; set; }
		public string DefaultValue { get; set; }
		public int Visibility { get; set; }
		public int? PageNumber { get; set; }
		public DateTime? LastUpdate { get; set; }
	}
}