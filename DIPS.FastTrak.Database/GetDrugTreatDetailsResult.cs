using System;
namespace DIPS.FastTrak.Database
{
	public class GetDrugTreatDetailsResult
	{
		public int TreatId { get; set; }
		public int PersonId { get; set; }
		public string ATC { get; set; }
		public int? ATCVersion { get; set; }
		public string DrugName { get; set; }
		public string DrugForm { get; set; }
		public string TreatType { get; set; }
		public string PackType { get; set; }
		public string TreatPackType { get; set; }
		public Decimal? Strength { get; set; }
		public string StrengthUnit { get; set; }
		public Decimal? Dose24hCount { get; set; }
		public Decimal? Dose24hDD { get; set; }
		public DateTime StartAt { get; set; }
		public int? StartFuzzy { get; set; }
		public string StartReason { get; set; }
		public string RxText { get; set; }
		public DateTime? StopAt { get; set; }
		public int? StopFuzzy { get; set; }
		public string StopReason { get; set; }
		public string DoseCode { get; set; }
		public int? PauseStatus { get; set; }
		public DateTime CreatedAt { get; set; }
		public int? DoseId { get; set; }
		public int? StartedBy { get; set; }
		public int? CreatedBy { get; set; }
		public int? StopBy { get; set; }
		public int? BatchId { get; set; }
		public int? SignedBy { get; set; }
		public DateTime? SignedAt { get; set; }
		public string SubstanceName { get; set; }
	}
}