using System;
namespace DIPS.FastTrak.Database
{
	public class GetStudyNamesResult
	{
		public int StudyId { get; set; }
		public string StudyName { get; set; }
	}
}