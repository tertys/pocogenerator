using System;
namespace DIPS.FastTrak.Database
{
	public class GetStudyGroupsResult
	{
		public int GroupId { get; set; }
		public string GroupName { get; set; }
	}
}