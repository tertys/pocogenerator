using System;
namespace DIPS.FastTrak.Database
{
	public class GetMyRolesResult
	{
		public int RoleId { get; set; }
		public string RoleName { get; set; }
		public string RoleCaption { get; set; }
		public string RoleInfo { get; set; }
	}
}