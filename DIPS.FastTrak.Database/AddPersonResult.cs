using System;
namespace DIPS.FastTrak.Database
{
	public class AddPersonResult
	{
		public int? PersonId { get; set; }
	}
}