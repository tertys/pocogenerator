using System;
namespace DIPS.FastTrak.Database
{
	public class GetStudyItemsResult
	{
		public int ItemId { get; set; }
		public string VarName { get; set; }
		public int ItemType { get; set; }
		public string UnitStr { get; set; }
		public string LabName { get; set; }
		public int? ThreadTypeId { get; set; }
	}
}