using System;
namespace DIPS.FastTrak.Database
{
	public class GetCaseListResult
	{
		public int PersonId { get; set; }
		public DateTime DOB { get; set; }
		public string FullName { get; set; }
		public string GroupName { get; set; }
		public string InfoText { get; set; }
		public int? HandledBy { get; set; }
		public string PrimaryContactSign { get; set; }
		public string PrimaryContactName { get; set; }
		public byte GenderId { get; set; }
		public string NationalId { get; set; }
	}
}