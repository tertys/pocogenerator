using System;
namespace DIPS.FastTrak.Database
{
	public class GetFormActionsResult
	{
		public int OrderNumber { get; set; }
		public int FormId { get; set; }
		public int PageConditionId { get; set; }
		public int MasterId { get; set; }
		public int DetailId { get; set; }
		public int? ComparisonType { get; set; }
		public int? MasterEnumVal { get; set; }
		public DateTime LastUpdate { get; set; }
	}
}