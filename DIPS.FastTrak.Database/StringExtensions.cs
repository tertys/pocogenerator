﻿namespace DIPS.FastTrak.Database
{
    public static class StringExtensions
    {
        public static bool HasValue(this string @this) => !string.IsNullOrEmpty(@this);
    }
}