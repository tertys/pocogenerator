using System;
namespace DIPS.FastTrak.Database
{
	public class GetClinFormListResult
	{
		public int EventNum { get; set; }
		public int FormId { get; set; }
		public int EventId { get; set; }
		public DateTime? EventTime { get; set; }
		public string FormTitle { get; set; }
		public string FormName { get; set; }
		public int ClinFormId { get; set; }
		public string FormStatus { get; set; }
		public byte FormComplete { get; set; }
		public string Comment { get; set; }
		public string CachedText { get; set; }
		public string StatusDesc { get; set; }
		public DateTime CreatedAt { get; set; }
		public DateTime? SignedAt { get; set; }
		public bool Archived { get; set; }
		public string CreatedBySign { get; set; }
		public int? CreatedByProfId { get; set; }
		public int CreatedBy { get; set; }
		public string SignedBySign { get; set; }
		public int? SignedByProfId { get; set; }
		public int? SignedBy { get; set; }
		public string SignedByProfNameHistoric { get; set; }
		public string CreatedByProfNameHistoric { get; set; }
	}
}