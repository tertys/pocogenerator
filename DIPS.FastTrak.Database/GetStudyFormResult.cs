using System;
namespace DIPS.FastTrak.Database
{
	public class GetStudyFormResult
	{
		public int StudyId { get; set; }
		public int FormId { get; set; }
		public string FormName { get; set; }
		public string FormTitle { get; set; }
		public string Subtitle { get; set; }
		public bool? CalculateInvalid { get; set; }
		public bool? RatingScale { get; set; }
		public string Copyright { get; set; }
		public int? ThreadTypeId { get; set; }
		public string Instructions { get; set; }
		public bool? Repeatable { get; set; }
		public string SurveyStatus { get; set; }
		public int? FormActive { get; set; }
		public byte? HideComment { get; set; }
		public int? ParentId { get; set; }
		public DateTime? CreatedAt { get; set; }
		public int? CreatedBy { get; set; }
	}
}