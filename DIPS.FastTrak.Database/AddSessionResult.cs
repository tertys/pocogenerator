using System;
namespace DIPS.FastTrak.Database
{
	public class AddSessionResult
	{
		public Decimal? SessId { get; set; }
	}
}