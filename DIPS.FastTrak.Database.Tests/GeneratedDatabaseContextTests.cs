﻿using DIPS.FastTrak.Components;
using DIPS.FastTrak.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DIPS.FastTrak.Database.Tests
{
    [TestClass]
    public class GeneratedDatabaseContextTests
    {
        protected IFastTrakContext FastTrakContext { get; set; }
        protected IConnectionService ConnectionService { get; set; }

        [TestInitialize]
        public void TestInitialize()
        {
            ConnectionService = new ConnectionService();
            //FastTrakContext = new FastTrakContext(new SqlConnection("Data Source=localhost;initial catalog=GBD;Integrated Security=SSPI;"));
        }

        [TestClass]
        public class GetClinForms : GeneratedDatabaseContextTests
        {
            [TestMethod]
            public void ShouldReturnClinForms()
            {
                var clinForms = FastTrakContext.GetClinForms(3,5,null);
                //Assert
                //Assert.
                //clinForms.Success.
                //clinForms.ShouldNotBeEmpty();
            }
        }

        [TestClass]
        public class GetStudyNames : GeneratedDatabaseContextTests
        {
            [TestMethod]
            public void ShouldReturnStudyNames()
            {
                //var studyNames = FastTrakContext.GetStudyNames().ToList();
                //studyNames.ShouldNotBeEmpty();
            }
        }

        [TestClass]
        public class GetMyRoles : GeneratedDatabaseContextTests
        {
            [TestMethod]
            public void ShouldReturnMyRoles()
            {
                //var roles = FastTrakContext.GetMyRoles().ToList();
                //roles.ShouldNotBeEmpty();
            }
        }

        [TestClass]
        public class GetMetaForms : GeneratedDatabaseContextTests
        {
            [TestMethod]
            public void ShouldReturnMetaForms()
            {
                //var gbd = FastTrakContext.GetStudyNames().FirstOrDefault(s=>s.StudyName == "GBD");
                //gbd.ShouldNotBeNull();
                //var metaForms = FastTrakContext.GetMetaForms(gbd.StudyId);
                //metaForms.ShouldNotBeEmpty();
            }
        }

        [TestClass]
        public class GetLabData : GeneratedDatabaseContextTests
        {
            [TestMethod]
            public void ShouldReturnLabData()
            {
                //var labData = FastTrakContext.GetLabData(5).ToList();
                //labData.ShouldNotBeEmpty();
            }
        }

        [TestClass]
        public class GetLabNames : GeneratedDatabaseContextTests
        {
            [TestMethod]
            public void ShouldReturnGetLabNames()
            {
                //var labNames = FastTrakContext.GetLabNames().ToList();
                //labNames.ShouldNotBeEmpty();
            }
        }
    }
}
