﻿using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using CommandLine;
using MoreLinq.Extensions;
using PocoGenerator.Services;
using PocoGenerator.Components;
using PocoGenerator.Models;
using SimpleInjector;

namespace PocoGenerator
{
    static class Program
    {
        private static void Main(string[] args)
        {
            Parser.Default.ParseArguments<Options>(args)
                .WithParsed(RunOptionsAndReturnExitCode)
                .WithNotParsed(HandleParseError);
        }

        private static void HandleParseError(IEnumerable<Error> errs)
        {
            errs.ForEach(e => Console.WriteLine(e.ToString()));
            Environment.Exit(-1);
        }

        private static void RunOptionsAndReturnExitCode(Options opts)
        {
            var pocoGenerator = Bootstrap(opts).GetInstance<IPocoGeneratorService>();
            pocoGenerator.CreateSource();
            pocoGenerator.AddSourceToProjectFile();
        }

        private static Container Bootstrap(Options opts)
        {
            var container = new Container();
            container.Register(() => opts);
            container.Register(() => new SqlConnection($"Data Source={opts.Server};Initial Catalog={opts.DatabaseName};Integrated Security=true"), Lifestyle.Singleton);
            container.Register<IPocoGeneratorService, PocoGeneratorService>();
            container.Verify();
            return container;
        }
    }
}

