﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoreLinq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UlrikenUtils;
using UlrikenUtils.Helpers;

namespace PocoGenerator.Models
{
    public class Schemas
    {
        public Dictionary<string,List<string>> StoredProcedures { get; }

        public Schemas(FileName fileName)
        {
            Ensure.That(fileName).IsNotNull();
            Ensure.That(File.Exists(fileName.Value)).IsTrue();
            StoredProcedures = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(File.ReadAllText(fileName.Value));
        }
    }
}
