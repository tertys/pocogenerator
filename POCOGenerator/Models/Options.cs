﻿using CommandLine;

namespace PocoGenerator.Models
{
    public class Options
    {
        [Option('o', "outdir", Required = false, Default= @"..\..\..\DIPS.FastTrak.Database\", HelpText = "Output -location")]
        public string OutDir { get; set; }

        [Option('h', "host", Required = false, Default = "localhost", HelpText = "Database-server")]
        public string Server { get; set; }

        [Option('d', "database-name", Required = false, Default = "GBD", HelpText = "Database-name")]
        public string DatabaseName { get; set; }

        [Option('f', "proc-file", Required = true, Default = "StoredProcedures.json", HelpText = "The stored procedures to generate")]
        public string StoredProcFile { get; set; }
    }
}
