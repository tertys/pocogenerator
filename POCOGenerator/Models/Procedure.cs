﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Management.Smo;
using PocoGenerator.Helpers;
using MoreLinq;
using UlrikenUtils;
using UlrikenUtils.Helpers;

namespace PocoGenerator.Models
{
    /// <summary>
    /// Model for a Stored Procedure-details
    /// </summary>
    public class Procedure
    {
        #region Private Properties
        private string Name { get; set; }
        private SqlConnection SqlConnection { get; }
        private Dictionary<string, string> Parameters { get; set; } = new Dictionary<string, string>();
        private string ProcName => $"[{Schema}].[{Name}]";
        private string Schema { get; set; }
        private static string QueryString => "select column_ordinal, name, system_type_name, is_nullable from sys.dm_exec_describe_first_result_set ('$$procName$$',null, 0) order by column_ordinal";
        private List<ProcedureResultField> ProcedureResult { get; set; } = new List<ProcedureResultField>();
        #endregion

        #region Public Properties

        public string ResultClassName => $"{Name}Result";
        public bool HasResultClass { get; }
        public string ResultClass { get; }
        public string Signature { get; }
        public string Method { get; }

        #endregion

        #region Constructor
        public Procedure(StoredProcedure storedProcedure, SqlConnection sqlConnection)
        {
            Ensure.That(storedProcedure, sqlConnection).NoneAreNull();

            SqlConnection = sqlConnection;
            Schema = storedProcedure.Schema;
            Name = storedProcedure.Name;

            foreach (Parameter parameter in storedProcedure.Parameters)
            {
                Parameters.Add(parameter.Name, parameter.DataType + (parameter.DefaultValue == "NULL" ? "?" : ""));
            }

            GetProcedureResults();
            ResultClass = ExtractResultClass();
            HasResultClass = !string.IsNullOrWhiteSpace(ResultClass);
            Signature = GetSignature();
            Method = ToString();
        }
        #endregion

        #region Public Methods

        public sealed override string ToString()
        {
            var parameters = string.Join(",", Parameters.Select((p, i) => p.Key));
            var values = string.Join(",", Parameters.Select(p => p.Key.Strip().ToSqlParameter(p.Value.EndsWith("?"))));

            if (!string.IsNullOrWhiteSpace(values)) values = "," + values;

            var returnType = ProcedureResult.Any() ? $"Result<List<{ResultClassName}>>" : "Result<int>";
            var parameterList = string.Join(",", Parameters.Select(p => p.Value.ExtractType(p.Value.Contains("?")) + " " + p.Key.TrimStart('@')).ToList());
            var declaration = $"\tpublic {returnType} {Name} (" + parameterList + ")";
            var body = "\t\t\t=> " +
                (ProcedureResult.Any()
                    ? $@"Do.Try(()=>Database.SqlQuery<{ResultClassName}>(""{ProcName} {parameters}""{values}).ToList());"
                    : $@"Do.Try(()=>Database.ExecuteSqlCommand(""{ProcName} {parameters}""{values}));");

            var sb = new StringBuilder();
            sb.AppendLine(declaration);
            sb.AppendLine(body);
            return sb.ToString();
        }

        private void GetProcedureResults()
        {
            var command = new SqlCommand(QueryString.Replace("$$procName$$", $"{Schema}.{Name}"), SqlConnection);
            if (SqlConnection.State != System.Data.ConnectionState.Open) SqlConnection.Open();
            var reader = command.ExecuteReader();
            while (reader.Read())
            {
                var field = ProcedureResultField.Create(reader);
                if (field != null)
                    ProcedureResult.Add(field);
            }
            reader.Close();
            ProcedureResult = ProcedureResult.DistinctBy(p => p.Name).ToList();
        }

        private string ExtractResultClass()
        {
            if (!ProcedureResult.Any()) return "";

            var sb = new StringBuilder();
            sb.AppendLine("\t" + $"public class {ResultClassName}");
            sb.AppendLine("\t" + "{");
            ProcedureResult.ForEach(p => sb.AppendLine("\t\t" + p.ToString()));
            sb.AppendLine("\t}");
            return sb.ToString();
        }

        private string GetSignature()
        {
            var returnType = ProcedureResult.Any() ? $"Result<List<{ResultClassName}>>" : "Result<int>";
            var para = Parameters.Select(p => p.Value.ExtractType(p.Value.Contains("?")) + " " + p.Key.TrimStart('@')).ToList();
            var result = $"\t\t{returnType} {Name} (" + string.Join(",", para) + ");";
            return result;
        }
        #endregion
    }
}
