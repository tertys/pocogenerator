﻿using PocoGenerator.Helpers;
using System.Data.SqlClient;

namespace PocoGenerator.Models
{
    public class ProcedureResultField
    {
        public int ColumnOrdinal { get; set; }
        public string Name { get; set; }
        public string SystemTypeName { get; set; }
        public bool IsNullable { get; set; }

        private ProcedureResultField(SqlDataReader sqlData)
        {
            ColumnOrdinal = (int)sqlData["column_ordinal"];
            Name = (string)sqlData["name"];
            SystemTypeName = (string)sqlData["system_type_name"];
            IsNullable = (bool)sqlData["is_nullable"];
        }

        public override string ToString() => "public " + SystemTypeName.ExtractType(IsNullable) + " " + Name + " { get; set; }";

        public static ProcedureResultField Create(SqlDataReader sqlData)
        {
            if (sqlData["column_ordinal"] is System.DBNull || 
                sqlData["name"] is System.DBNull || 
                sqlData["system_type_name"] is System.DBNull || 
                sqlData["is_nullable"] is System.DBNull) return null;
            return new ProcedureResultField(sqlData);
        }
    }
}
