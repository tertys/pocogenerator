﻿namespace PocoGenerator.Models
{
    public abstract class StringWrap
    {
        protected StringWrap(string value)
        {
            Value = value;
        }
        public string Value { get; set; }
    }
}
