﻿using System.Data.SqlClient;
using PocoGenerator.Services;
using UlrikenUtils;
using UlrikenUtils.Components;
using UlrikenUtils.Helpers;

namespace PocoGenerator.Components
{
    public class ConnectionService : IConnectionService
    {
        private SqlConnection SqlConnection { get; }

        public ConnectionService(string server, string database) => SqlConnection = new SqlConnection($"Database={database};Server={server};Integrated Security=True;connect timeout = 30");

        public Result<SqlConnection> Connect()
            => Do.Try(() =>
            {
                SqlConnection.Open();
                return SqlConnection;
            });
    }
}
