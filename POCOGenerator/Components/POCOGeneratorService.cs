﻿using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using PocoGenerator.Services;
using System.Collections.Generic;
using System.Data.SqlClient;
using PocoGenerator.Models;
using PocoGenerator.Helpers;
using System.Linq;
using System.IO;
using System.Reflection;
using MoreLinq;
using Microsoft.Build.Evaluation;
using UlrikenUtils.Components;
using UlrikenUtils.Helpers;

namespace PocoGenerator.Components
{
    public class PocoGeneratorService : IPocoGeneratorService
    {
        #region Private properties

        private static string ResultClassTemplate =>
            "using System;\n" +
            "namespace DIPS.FastTrak.Database\n" +
            "{\n" +
            "<body>" +
            "}";

        private static string ComponentClassTemplate =>
            "using System;\n" +
            "using System.Collections.Generic;\n" +
            "using System.Data.Entity;\n" +
            "using System.Data.SqlClient;\n" +
            "using System.Linq;\n" +
            "using DIPS.FastTrak.Database;\n" +
            "using DIPS.FastTrak.Services;\n" +
            "using Optional.Unsafe;\n" +
            "using UlrikenUtils;\n" +
            "// ReSharper disable InconsistentNaming\n" +
            "// ReSharper disable CheckNamespace\n\n" +
            "namespace DIPS.FastTrak.Components\n{\n" +
            "\tpublic class FastTrakContext : DbContext, IFastTrakContext\n\t{\n" +

            "\t\tpublic FastTrakContext(IConnectionService connectionService)\n" +
            "\t\t\t: base(GetSqlConnection(connectionService), true){}\n\n" +
            "\t\tprivate static SqlConnection GetSqlConnection(IConnectionService connectionService)\n" +
            "\t\t{\n" +
            "\t\t\tvar connection = connectionService.Connect();\n" +
            "\t\t\tEnsure.That(connection.Success, \"Unable to create a connection to the database!\").IsTrue();\n" +
            "\t\t\tEnsure.That(connection.Value.HasValue, \"No SqlConnection generated, this is fatal\").IsTrue();\n" +
            "\t\t\treturn connection.Value.ValueOrDefault();\n" +
            "\t\t}\n\n" +
            "\t\tpublic virtual Result<List<GetPopulationResult>> GetPopulation(int studyId, string sqlText)\n" +
            "\t\t\t=> Do.Try(()=>Database.SqlQuery<GetPopulationResult>($\"{sqlText}\".Replace(\":\", \"@\"), " +
            "new SqlParameter(\"StudyId\", studyId)).ToList());\n\n" +
            "<body>   }\n}";

        private static string ServiceTemplate =>
            "using System;\n" +
            "using System.Collections.Generic;\n" +
            "using UlrikenUtils;\n\n" +
            "namespace DIPS.FastTrak.Database\n{\n" +
            "\tpublic interface IFastTrakContext\n\t{\n" +
            "\t\tResult<List<GetPopulationResult>> GetPopulation(int studyId, string sqlText);\n" +
            "<body>\n\t}\n}";

        private SqlConnection SqlConnection { get; }
        private DirectoryInfo OutDir { get; }
        private Schemas Schemas { get; }
        private Database Database { get; }

        #endregion

        #region Constructor
        public PocoGeneratorService(Options options)
        {
            SqlConnection = new SqlConnection($"Data Source={options.Server};Initial Catalog={options.DatabaseName};Integrated Security=true");
            OutDir = new DirectoryInfo(options.OutDir);
            Schemas = new Schemas(new FileName(options.StoredProcFile));
            var server = new Server(new ServerConnection(SqlConnection));
            Database = server.Databases[options.DatabaseName];
            if (!OutDir.Exists) OutDir.Create();
        }
        #endregion

        #region Private Methods

        private Result CreateResultClasses(List<Procedure> storedProcedures)
            => Do.Try(() =>
            {
                storedProcedures.Where(s => s.HasResultClass).ForEach(s =>
                {
                    File.WriteAllText(
                        Path.Combine(OutDir.FullName, s.ResultClassName + ".cs"),
                        ResultClassTemplate.Replace("<body>", s.ResultClass));
                });

                File.WriteAllText(
                    Path.Combine(OutDir.FullName, "GetPopulationResult.cs"),
                    "using System;\n\n" +
                    "namespace DIPS.FastTrak.Database\n{\n\tpublic class GetPopulationResult\n{\n" +
                    "\t\tpublic int PersonId { get; set; }\n" +
                    "\t\tpublic DateTime DOB { get; set; }\n" +
                    "\t\tpublic string FullName { get; set; }\n" +
                    "\t\tpublic string GroupName { get; set; }\n" +
                    "\t\tpublic string InfoText { get; set; }\n\t}\n}");
            });

        private Result CreateFastTrakComponent(List<Procedure> storedProcedures)
            => Do.Try(() =>
            {
                var body = string.Join("\n", storedProcedures.Select(s => "\t" + s.Method).ToList());
                var path = Path.Combine(OutDir.FullName, "..\\DIPS.FastTrak.Components", $"FastTrakContext.cs");
                File.WriteAllText(path, ComponentClassTemplate.Replace("<body>", body));
                File.Delete(Path.Combine(OutDir.FullName, $"FastTrakContext.cs"));
            });

        private Result CreateFastTrakService(List<Procedure> storedProcedures) => Do.Try(() =>
        {
            var body = string.Join("\n", storedProcedures.Select(s => s.Signature).ToList());
            File.WriteAllText(
                Path.Combine(OutDir.FullName, $"IFastTrakContext.cs"),
                ServiceTemplate.Replace("<body>", body));
        });

        #endregion

        #region Implementation of IPocoGeneratorService

        public Result CreateSource() => Do.Try(() =>
        {
            var includeStoredProcedures = Schemas.StoredProcedures.SelectMany(k => k.Value.Select(v => $"{k.Key}.{v}"));

            var storedProcedures = Database.StoredProcedures.AsList()
                .Where(p => includeStoredProcedures.Contains($"{p.Schema}.{p.Name}"))
                .Select(p => new Procedure(p, SqlConnection))
                .ToList();

            CreateResultClasses(storedProcedures);
            CreateFastTrakComponent(storedProcedures);
            CreateFastTrakService(storedProcedures);
        });

        public Result AddSourceToProjectFile() => Do.Try(() =>
        {
            ProjectCollection.GlobalProjectCollection.UnloadAllProjects();
            var generatedSourceFiles = OutDir.EnumerateFiles("*.cs").Where(f => f.Name != "FastTrakContext.cs").ToList();
            File.Delete(Path.Combine(OutDir.FullName, "DIPS.FastTrak.Database.csproj"));
            File.Copy(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "ProjectFileTemplate.txt"), Path.Combine(OutDir.FullName, "DIPS.FastTrak.Database.csproj"));
            var databaseProject = new Project(Path.Combine(OutDir.FullName, "DIPS.FastTrak.Database.csproj"));
            databaseProject.RemoveItems(databaseProject.Items.Where(i => i.ItemType == "Compile"));
            generatedSourceFiles.ForEach(f => databaseProject.AddItem("Compile", f.Name));
            databaseProject.Save();
        });

        #endregion
    }
}
