﻿using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using PocoGenerator.Services;
using UlrikenUtils;
using UlrikenUtils.Components;
using UlrikenUtils.Helpers;

namespace PocoGenerator.Components
{
    public class UdlConnectionService : IConnectionService
    {
        private SqlConnection SqlConnection { get; }

        public UdlConnectionService(FileInfo udlFile)
        {
            Ensure.That(udlFile.Exists, "Please provide an existing UDL-file!");
            var udlInfo = new OleDbConnection($"File Name={udlFile.FullName}");
            SqlConnection =
                new SqlConnection(
                    $"Database={udlInfo.Database};Server={udlInfo.DataSource};Integrated Security=True;connect timeout = 30");
        }

        public Result<SqlConnection> Connect()
            => Do.Try(() =>
            {
                SqlConnection.Open();
                return SqlConnection;
            });
    }
}