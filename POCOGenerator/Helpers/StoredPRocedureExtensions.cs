﻿using Microsoft.SqlServer.Management.Smo;
using System.Collections.Generic;

namespace PocoGenerator.Helpers
{
    public static class StoredProcedureExtensions
    {
        public static List<StoredProcedure> AsList(this StoredProcedureCollection @this)
        {
            var list = new List<StoredProcedure>();
            foreach (StoredProcedure storedProcedure in @this)
            {
                list.Add(storedProcedure);
            }
            return list;
        }
    }
}
