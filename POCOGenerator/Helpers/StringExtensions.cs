﻿using System;

namespace PocoGenerator.Helpers
{
    public static class StringExtensions
    {
        public static bool HasValue(this string @this) => !string.IsNullOrEmpty(@this);

        public static string FirstUpperCase(this string @this)
            => @this.Substring(0, 1).ToUpper() + @this.Substring(1);

        public static string ToSqlParameter(this string @this, bool isNullable)
            => isNullable
                ? $"{@this}.HasValue ? new SqlParameter(\"{@this}\",{@this}) : new SqlParameter(\"{@this}\", DBNull.Value)"
                : $"new SqlParameter(\"{@this}\", {@this})";

        public static string Strip(this string @this)
            => @this.Replace("@","");

        public static string ExtractType(this string @this, bool isNullable)
        {
            if (@this.Contains("varchar")) return "string";
            if (@this.Contains("char")) return "string";
            if (@this.Contains("bit")) return isNullable ? "bool?" : "bool";
            if (@this.Contains("tinyint")) return isNullable ? "byte?" : "byte";
            if (@this.Contains("int")) return isNullable ? "int?" : "int";
            if (@this.Contains("uniqueidentifier")) return isNullable ? "Guid?" : "Guid";
            if (@this.Contains("datetime")) return isNullable ? "DateTime?" : "DateTime";
            if (@this.Contains("decimal")) return isNullable ? "Decimal?" : "Decimal";
            if (@this.Contains("float")) return isNullable ? "Double?" : "Double";
            if (@this.Contains("numeric")) return isNullable ? "Decimal?" : "Decimal";

            return @this;
        }
    }
}
