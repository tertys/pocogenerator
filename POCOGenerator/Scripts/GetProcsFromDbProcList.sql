﻿--select 'insert into dbo.DbProcList (ProcId, ListId, ProcName, ProcDesc) ' + 
--' values (' + CAST(ProcId as varchar) + ',''API'',''' + ProcName + ''',''' + ProcDesc + ''');' 
--from DbProcList where ListId = 'API'

insert into dbo.DbProcList (ProcId, ListId, ProcName, ProcDesc)  values (20001,'API','dbo.GetStudyNames','Get studies');
insert into dbo.DbProcList (ProcId, ListId, ProcName, ProcDesc)  values (20002,'API','dbo.AddMyself','Add myself as user');
insert into dbo.DbProcList (ProcId, ListId, ProcName, ProcDesc)  values (20003,'API','dbo.UpdateCaseGroup','UpdateCaseGroup');
insert into dbo.DbProcList (ProcId, ListId, ProcName, ProcDesc)  values (20004,'API','dbo.GetStudyGroups','GetStudyGroups');
insert into dbo.DbProcList (ProcId, ListId, ProcName, ProcDesc)  values (20005,'API','dbo.GetCaseList','GetCaseList');
insert into dbo.DbProcList (ProcId, ListId, ProcName, ProcDesc)  values (20006,'API','Populations.GetStudyPopulations','GetStudyPopulations');
insert into dbo.DbProcList (ProcId, ListId, ProcName, ProcDesc)  values (20007,'API','AccessCtrl.GetMyRoles','AccessCtrl.GetMyRoles');
insert into dbo.DbProcList (ProcId, ListId, ProcName, ProcDesc)  values (20008,'API','dbo.GetUserDetails','dbo.GetUserDetails');
insert into dbo.DbProcList (ProcId, ListId, ProcName, ProcDesc)  values (20009,'API','CRF.GetMetaForms','CRF.GetMetaForms');
insert into dbo.DbProcList (ProcId, ListId, ProcName, ProcDesc)  values (20010,'API','dbo.GetDrugTreatDetails','dbo.GetDrugTreatDetails');
insert into dbo.DbProcList (ProcId, ListId, ProcName, ProcDesc)  values (20011,'API','dbo.GetLabData','dbo.GetLabData');
insert into dbo.DbProcList (ProcId, ListId, ProcName, ProcDesc)  values (20012,'API','dbo.GetLabNames','dbo.GetLabNames');
insert into dbo.DbProcList (ProcId, ListId, ProcName, ProcDesc)  values (20013,'API','dbo.GetClinForms','dbo.GetClinForms');
insert into dbo.DbProcList (ProcId, ListId, ProcName, ProcDesc)  values (20014,'API','dbo.GetStudyAndUser','dbo.GetStudyAndUser');
insert into dbo.DbProcList (ProcId, ListId, ProcName, ProcDesc)  values (20015,'API','Populations.GetStudyPopulations','Populations.GetStudyPopulations');
insert into dbo.DbProcList (ProcId, ListId, ProcName, ProcDesc)  values (20016,'API','dbo.UpdateClinFormSetTime','dbo.UpdateClinFormSetTime');
