﻿using System.Data.SqlClient;
using UlrikenUtils;
using UlrikenUtils.Components;

namespace PocoGenerator.Services
{
    public interface IConnectionService
    {
        Result<SqlConnection> Connect();
    }
}