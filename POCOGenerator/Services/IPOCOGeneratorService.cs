﻿using UlrikenUtils;
using UlrikenUtils.Components;

namespace PocoGenerator.Services
{
    public interface IPocoGeneratorService
    {
        /// <summary>
        /// Fetch stored procedures defined in DbProcList, and generate c#-source.
        /// </summary>
        Result CreateSource();

        /// <summary>
        /// Replace the files in the database-project with the generated files
        /// </summary>
        Result AddSourceToProjectFile();
    }
}
